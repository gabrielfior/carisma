#!/usr/bin/python
#from __future__ import unicode_literals

from func_explanations import redirect_drive, call_eric, open_music_spotify
import datetime
import speech_recognition as sr
from recognize_speech import return_words
import os

#from search_music import search_music_return_uri,start_raspberry_fm

def wait_for_audio():

    """
    Runs function and waits until something can be interpreted.
    0 - Speech recognition : return_words() in recognize_speech.py
    Options:
        1 - drive - redirect_drive(value)
        2 - spotify - open_music_spotify(song_name)
        3 - call - call_eric()
    :rtype:
    """


    #Connect to Bluetooth - dont need, already connected at this time
    #os.system('echo -e "connect F8:95:C7:73:7A:BB\nquit" | bluetoothctl')

    #Start FM Transmitter(ToDo)

    value = return_words()

    if 'drive' in value or 'navigate' in value or 'navigation' in value:

        print "entering navigation"
        redirect_drive(value)

    elif 'spotify' in value or 'play' in value:
        #one should say: play Love Yourself on Spotify
        try:
            song_name=value[value.index('play')+len('play')+1:value.index('on')-1]
            print song_name
            open_music_spotify(song_name)

        except ValueError:
            print "Error in search spotify"
    elif 'call' in value:
        call_eric()


if __name__ == "__main__":
    wait_for_audio()