#!/usr/bin/python
#from __future__ import unicode_literals
import speech_recognition as sr
import os

r = sr.Recognizer()
m = sr.Microphone()
#track = self.session.get_track('spotify:track:3Eppml08js3A6aRUrcLh08')
try:
    print("A moment of silence, please...")
    with m as source: r.adjust_for_ambient_noise(source)
    print("Set minimum energy threshold to {}".format(r.energy_threshold))
    count=0
    while True:
        print("Say something!")
        count +=1
        print count
        with m as source: audio = r.listen(source)
        print("Got it! Now to recognize it...")
        try:
            # recognize speech using Google Speech Recognition
            value = r.recognize_google(audio)

            # we need some special handling here to correctly print unicode characters to standard output
            if str is bytes: # this version of Python uses bytes for strings (Python 2)
                words=str(u"You said {}".format(value).encode("utf-8"))[9:]
                print "here it comes"
                print words
                print(u"You said {}".format(value).encode("utf-8"))
                if 'spotify' in value or 'play' in value:
                    os.system('adb shell am start -a android.intent.action.VIEW spotify:track:6xZtSE6xaBxmRozKA0F6TA')

            else: # this version of Python uses unicode for strings (Python 3+)
                print("You said {}".format(value))
                if 'drive' in value or 'navigate' in value or 'navigation' in value:
                    #value=value.replace('navigate ', ' ')
                    #value=value.replace(' ', '+')
                    #os.system('adb shell am start -a "android.intent.action.VIEW" -d "google.navigation:q=' + value+ '"')
                    print "hit"
                    os.system('adb shell am start -a "android.intent.action.VIEW" -d "google.navigation:q=Munich+Central+Station"')
                elif 'spotify' in value or 'play' in value:
                    os.system('adb shell am start -a android.intent.action.VIEW spotify:track:6xZtSE6xaBxmRozKA0F6TA')
        except sr.UnknownValueError:
            print("Oops! Didn't catch that")
        except sr.RequestError as e:
            print("Uh oh! Couldn't request results from Google Speech Recognition service; {0}".format(e))
except KeyboardInterrupt:
    pass
