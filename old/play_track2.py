import spotify
import threading

class Music:
    session = None

    def __init__(self):
        logged_in_event = threading.Event()

        def connection_state_listener(session):
            if session.connection.state is spotify.ConnectionState.LOGGED_IN:
                logged_in_event.set()

        self.session = spotify.Session()
        loop = spotify.EventLoop(self.session)
        loop.start()
        self.session.on(
                   spotify.SessionEvent.CONNECTION_STATE_UPDATED,
                   connection_state_listener)

        self.session.login('accountname', 'password')
        logged_in_event.wait()

        print self.session.connection.state
        print self.session.user

    def play(self):
        track = self.session.get_track('spotify:track:3Eppml08js3A6aRUrcLh08')
        track.load()

	audio = spotify.AlsaSink(self.session)
        self.session.player.load(track)
        self.session.player.play(play=True)
