import os
from search_music import search_music_return_uri

def redirect_drive(value):

    print ('Entering redirect drive function')
    os.system('adb shell am start -a "android.intent.action.VIEW" -d "google.navigation:q=Barer+Str+47+Munich"')
    return "End redirect drive"

def call_eric():
    os.system('adb shell am start -a android.intent.action.CALL -d tel:017656996452')
    return "End call eric"

def open_music_spotify(song_name1):

    uri = search_music_return_uri(song_name1)
    os.system('adb shell am start -a android.intent.action.VIEW ' + uri)
    return "End open music spotify"