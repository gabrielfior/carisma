import googlemaps
from datetime import datetime

key = 'AIzaSyD4-QFNuuZkYyevVOHnidV_LKeF11QE_kE'
gmaps = googlemaps.Client(key=key)

# Geocoding an address
geocode_result = gmaps.geocode('1600 Amphitheatre Parkway, Mountain View, CA')

# Look up an address with reverse geocoding
reverse_geocode_result = gmaps.reverse_geocode((40.714224, -73.961452))

# Request directions via public transit
now = datetime.now()
directions_result = gmaps.directions("TUM Department of Informatics, Boltzmannstrasse 3, 85748 Garching",
                                     "Barer Strasse 47, 80799 Munich",
                                     mode="transit",
                                     departure_time=now)
#mode=Valid values are "bus", "subway", "train", "tram", "rail".
#raspi = '169.254.243.61'

#>>> directions_result[0]['bounds']
#{u'northeast': {u'lat': 48.8018092, u'lng': 11.558335}, u'southwest': {u'lat': 48.140232, u'lng': 9.181635}}

#directions_result[0]['legs'][0]['steps'][0]['html_instructions']
#u'Walk to Garching Forschungszentrum'

for i in directions_result[0]['legs'][0]['steps']:

    print i['html_instructions']

def return_commands(words):

    key = 'AIzaSyD4-QFNuuZkYyevVOHnidV_LKeF11QE_kE'
    gmaps = googlemaps.Client(key=key)

    now = datetime.now()
    directions_result = gmaps.directions("TUM Department of Informatics, Boltzmannstrasse 3, 85748 Garching",
                                     #"Barer Strasse 47, 80799 Munich",
                                    words,
                                     mode="transit",
                                     departure_time=now)

    list_dir=[]
    for i in directions_result[0]['legs'][0]['steps']:

        list_dir.append(i['html_instructions'])

    return list_dir