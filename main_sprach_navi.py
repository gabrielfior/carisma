#!/usr/bin/python
#from __future__ import unicode_literals
import speech_recognition as sr
import os
from search_music import search_music_return_uri
r = sr.Recognizer()
m = sr.Microphone()

try:
    print("A moment of silence, please...")
    with m as source: r.adjust_for_ambient_noise(source)
    print("Set minimum energy threshold to {}".format(r.energy_threshold))
    while True:

        print("Say something!")
        with m as source: audio = r.listen(source)
        print("Got it! Now to recognize it...")
        try:
            # recognize speech using Google Speech Recognition
            value = r.recognize_google(audio)

            # we need some special handling here to correctly print unicode characters to standard output
            if str is bytes: # this version of Python uses bytes for strings (Python 2)
                words=str(u"You said {}".format(value).encode("utf-8"))[9:]
                words = words.lower()
                value = value.lower()
                print "here it comes"
                print words
                print(u"You said {}".format(value).encode("utf-8"))
            else: # this version of Python uses unicode for strings (Python 3+)
                print("You said {}".format(value))

            if 'drive' in value or 'navigate' in value or 'navigation' in value:

                #value=value.replace('navigate ', ' ')
                #value=value.replace(' ', '+')
                #os.system('adb shell am start -a "android.intent.action.VIEW" -d "google.navigation:q=' + value+ '"')
                print "hit"
                #Was sagt sie?
                #bring me to barer str?

                nav = value
                os.system('adb shell am start -a "android.intent.action.VIEW" -d "google.navigation:q=Barer+Str+47+Munich"')
            elif 'spotify' in value or 'play' in value:
                #one should say: Carisma play Love Yourself on Spotify
                try:
                    song_name=value[value.index('play')+len('play')+1:value.index('on')-1]
                    uri = search_music_return_uri(song_name)
                    #uri = 'spotify:track:6xZtSE6xaBxmRozKA0F6TA'
                    os.system('adb shell am start -a android.intent.action.VIEW ' + uri)
                except ValueError:
                    pass
            elif 'call' in value:
                os.system('adb shell am start -a android.intent.action.CALL -d tel:017656996452')

        except sr.UnknownValueError:
            print("Oops! Didn't catch that")
        except sr.RequestError as e:
            print("Uh oh! Couldn't request results from Google Speech Recognition service; {0}".format(e))


    with m as source: r.adjust_for_ambient_noise(source)
    print 'adjusted source'
except KeyboardInterrupt:
    pass
